# swagger-editor
docker stop my_swagger && docker rm my_swagger
docker run -d -p 10083:8080 \
-e BASE_URL=/ \
-v /data/swagger:/swagger_api \
-e SWAGGER_FILE=/swagger_api/aoi.json \
--name my_swagger_editor \
swaggerapi/swagger-editor
docker ps | grep swa

# swagger_ui
docker stop my_swagger_ui && docker rm my_swagger_ui
docker run -d -p 10082:8080 \
-e SWAGGER_JSON=/swagger_api/aoi.yaml \
-v /data/swagger:/swagger_api \
--name my_swagger_ui \
swaggerapi/swagger-ui
docker ps | grep swa

http://123.60.231.101:10082/