import connexion
import six

from swagger_server.models.user import User  # noqa: E501
from swagger_server import util
from flask_cors import *


def create_user(body):  # noqa: E501
    """Create user

    This can only be done by the logged in user. # noqa: E501

    :param body: Created user object
    :type body: dict | bytes

    :rtype: None
    """
    if connexion.request.is_json:
        body = User.from_dict(connexion.request.get_json())  # noqa: E501
    rsp = {
        'code': '0000',
        'msg': 'succeed',
        'data': [
            {
                "id": body.id,
                "username": body.username,
                "firstName": body.first_name,
                "lastName": body.last_name,
                "email": body.email,
                "password": body.email,
                "phone": body.phone,
                "userStatus": 0
            }
        ]
    }
    return rsp


def create_users_with_array_input(body):  # noqa: E501
    """Creates list of users with given input array

     # noqa: E501

    :param body: List of user object
    :type body: list | bytes

    :rtype: None
    """
    if connexion.request.is_json:
        body = [User.from_dict(d) for d in connexion.request.get_json()]  # noqa: E501
    return 'do some magic!'


def create_users_with_list_input(body):  # noqa: E501
    """Creates list of users with given input array

     # noqa: E501

    :param body: List of user object
    :type body: list | bytes

    :rtype: None
    """
    if connexion.request.is_json:
        body = [User.from_dict(d) for d in connexion.request.get_json()]  # noqa: E501
    return 'do some magic!'


def delete_user(username):  # noqa: E501
    """Delete user

    This can only be done by the logged in user. # noqa: E501

    :param username: The name that needs to be deleted
    :type username: str

    :rtype: None
    """
    return 'do some magic!'


@cross_origin()
def get_user_by_name(username):  # noqa: E501
    """Get user by user name

     # noqa: E501

    :param username: The name that needs to be fetched. Use user1 for testing. 
    :type username: str

    :rtype: User
    """
    user = User.from_dict({
        "id": 0,
        "username": "LiuBei",
        "firstName": "Liu",
        "lastName": "Bei",
        "email": "001@gov.shu",
        "password": "222",
        "phone": "12332423",
        "userStatus": 0
    })
    rsp = {
        'code': '0000',
        'msg': 'succeed',
        'data': [
            user
        ]
    }
    return rsp


@cross_origin()
def get_users():  # noqa: E501
    user1 = User.from_dict({
        "id": 0,
        "username": "LiuBei",
        "firstName": "Liu",
        "lastName": "Bei",
        "email": "001@gov.shu",
        "password": "222",
        "phone": "12332423",
        "userStatus": 0
    })
    user2 = User.from_dict({
        "id": 0,
        "username": "LiuBei",
        "firstName": "Liu",
        "lastName": "Bei",
        "email": "001@gov.shu",
        "password": "222",
        "phone": "12332423",
        "userStatus": 0
    })
    rsp = {
        'code': '0000',
        'msg': 'succeed',
        'data': [
            user1,
            user2
        ]
    }
    return rsp

def login_user(username, password):  # noqa: E501
    """Logs user into the system

     # noqa: E501

    :param username: The user name for login
    :type username: str
    :param password: The password for login in clear text
    :type password: str

    :rtype: str
    """
    return 'do some magic!'


def logout_user():  # noqa: E501
    """Logs out current logged in user session

     # noqa: E501


    :rtype: None
    """
    return 'do some magic!'


def update_user(username, body):  # noqa: E501
    """Updated user

    This can only be done by the logged in user. # noqa: E501

    :param username: name that need to be updated
    :type username: str
    :param body: Updated user object
    :type body: dict | bytes

    :rtype: None
    """
    if connexion.request.is_json:
        body = User.from_dict(connexion.request.get_json())  # noqa: E501
    return 'do some magic!'
