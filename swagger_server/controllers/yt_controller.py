#!/usr/bin/env python
# _*_coding:utf-8_*_

"""
@Time : 2021/11/3 17:21
@Author: RunAtWorld
@File: yt_controller.py
"""


import connexion
from flask_cors import cross_origin


@cross_origin(supports_credentials=True)
def upload_infer_result(body):
    if connexion.request.is_json:
        body = connexion.request.get_json()
    if not body:
        return {
            "ErrorMsg": "req json is empty",
            "ErrorCode": "400",
        }
    if not body.get("ProjectName") or not body.get("TaskName"):
        return {
            "ErrorMsg": "ProjectName or TaskName is empty",
            "ErrorCode": "400",
        }
    task_type = body.get("TaskType")
    if not task_type:
        return {
            "ErrorMsg": "Please check the TaskType",
            "ErrorCode": "400",
        }
    rsp = {
        "status": "200",
        "msg": "success",
        "data": None
    }
    return rsp

@cross_origin(supports_credentials=True)
def ok_ng_count():
    rsp = {
        "status": "200",
        "msg": "success",
        "data": {
            "ok": 70,
            "ng": 27,
            "--": 3
            }
    }
    return rsp
