#!/usr/bin/env python
# _*_coding:utf-8_*_

"""
@Time : 2021/11/3 17:21
@Author: RunAtWorld
@File: infer_controller.py
"""
import json
import os

import connexion
from flask_cors import cross_origin

project_dict = {
    "det": "det",
    "det_abnomal": "det_abnomal",
    "det_cls_template": "det_cls_template",
    "ocr": "ocr_ctpn_crnn",
    "seg": "unet_WithReg",
    "ssd": "ssd",
    "ssd_resnet-WithoutReg": "det_cls_template",
    "ssd_resnet-WithReg": "ssd_resnet_WithReg",
    "crnn": "ocr_crnn",
    "ctpn_crnn": "ocr_ctpn_crnn",
    "unet-SetTemplate": "unet_setTemplate",
    "unet-WithReg": "unet_WithReg",
    "unet-WithoutReg": "unet_WithoutReg",
    "det_plus_det": "det_det",
    "det_plus_det_cls": "det_det_cls",
    "det_plus_ocr": "det_ocr",
    "det_plus_seg": "det_seg"
}


@cross_origin(supports_credentials=True)
def infer_det_plus(body):
    """
    infer
    """
    print("content_type:", connexion.request.content_type)
    print("req:", connexion.request.values)
    if connexion.request.is_json:
        body = connexion.request.get_json()
    print("req:", body)
    if not body:
        return {
            "ErrorMsg": "req json is empty",
            "ErrorCode": "400",
        }
    if not body.get("ProjectName") or not body.get("TaskName"):
        return {
            "ErrorMsg": "ProjectName or TaskName is empty",
            "ErrorCode": "400",
        }
    project_name = body.get("ProjectName")
    task_type = body.get("TaskType")
    if not task_type:
        return {
            "ErrorMsg": "Please check the TaskType",
            "ErrorCode": "400",
        }
    if project_name in project_dict:
        file_name = "{0}-rsp".format(project_dict[project_name])
    else:
        file_name = "{0}-rsp".format(project_name)
    rsp_json_file_path = os.path.join(os.path.realpath("."), 'rsp_json/{0}.json'.format(file_name))
    print(rsp_json_file_path)
    try:
        with open(rsp_json_file_path, 'r') as rsp_file:
            rsp = json.load(rsp_file)

        if not project_name.startswith('crnn'):
            rsp_dict = dict(rsp)
            new_file_name = body.get("Files")[0].get("FileName")
            print("new_file_name:", new_file_name)
            for k, v in rsp_dict.items():
                rsp[new_file_name] = v
    except FileNotFoundError as e:
        print(rsp_json_file_path)
        print("{0}:{1}".format(rsp_json_file_path, e.strerror))
        rsp = {
            "ErrorMsg": e.strerror,
            "ErrorCode": "500",
        }
    return rsp


def infer_det_cls_withoutReg(body):
    return infer_det_plus(body)


def infer_det_cls_withoutReg2(body):
    return infer_det_plus(body)


def infer_det_cls_withReg(body):
    return infer_det_plus(body)


def infer_det_cls_det_det(body):
    return infer_det_plus(body)


def infer_det_cls_ssd(body):
    return infer_det_plus(body)


def infer_ocr_crnn(body):
    return infer_det_plus(body)


def infer_ocr_ctpn_crnn(body):
    return infer_det_plus(body)


def infer_ocr_ssd_crnn(body):
    return infer_det_plus(body)


def infer_seg_withoutReg(body):
    return infer_det_plus(body)


def infer_seg_setTemplate(body):
    return infer_det_plus(body)


def infer_seg_withReg(body):
    return infer_det_plus(body)


def infer_seg_det_seg(body):
    return infer_det_plus(body)
