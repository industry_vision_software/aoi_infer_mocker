#!/usr/bin/env python3

import connexion

from swagger_server import encoder

from flask_cors import CORS, cross_origin

def main():
    app = connexion.App(__name__, specification_dir='./swagger/')
    app.app.json_encoder = encoder.JSONEncoder
    app.add_api('aoi.yaml', arguments={'title': 'AI Infer Mocker with Swagger'})
    app.host='0.0.0.0'


    app.run(port=18093)



if __name__ == '__main__':
    main()
