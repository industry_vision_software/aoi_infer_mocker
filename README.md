# AI Infer Mocker with Swagger

AI 推理服务模拟

## Requirements
Python 3.5.2+

## 在服务器Python方式启动

在根目录下运行

```
pip3 install -r requirements.txt
python3 -m swagger_server
```

在浏览器打开UI界面
```
http://localhost:18093/v2/ui/
```

调用 推理模拟的接口
```
http://localhost:18093/infer
```

## 使用pycharm启动

![image-20211208112340410](pics/image-20211208112340410.png)

## 在服务器使用Docker

在根目录下执行

```bash
# building the image
docker build -t swagger_server .

# starting up a container
docker run -p 18093:18093 swagger_server
```

# 请求和响应示例

## 请求示例

1. [检测+OCR请求示例](json/example_detp_req.json)

## 响应示例

| 响应示例                                           | 示例图片                                        | 对应项目    | 对应任务    |
| -------------------------------------------------- | ----------------------------------------------- | ----------- | ----------- |
| [单检测](rsp_json/det-rsp.json)                    | [示例图片](./rsp_json/test_imgs/200dk-grey.jpg) | det         | det         |
| [获取检测模板](rsp_json/det_cls_template-rsp.json) | [示例图片](./rsp_json/test_imgs/200dk-grey.jpg) | det_cls_template | det_cls_template |
| [异物检测](rsp_json/det_abnomal-rsp.json) | [示例图片](./rsp_json/test_imgs/200dk-grey.jpg) | det_abnomal | det_abnomal |
| [ocr-crnn](rsp_json/ocr_crnn-rsp.json)             | -                                               | crnn | crnn |
| [ocr-ctpn_crnn](rsp_json/ocr_ctpn_crnn-rsp.json)   | [示例图片](./rsp_json/test_imgs/200dk-grey.jpg) | ctpn_crnn | ctpn_crnn |
| [unet设置模板](rsp_json/unet-SetTemplate-rsp.json) | -                                               | unet-SetTemplate | unet-SetTemplate |
| [unet带模板](rsp_json/unet_WithReg-rsp.json)       | [示例图片](./rsp_json/test_imgs/unet-grey.jpg)  | unet-WithReg | unet-WithReg |
| [unet不带模板](rsp_json/unet_WithoutReg-rsp.json)  | [示例图片](./rsp_json/test_imgs/unet-grey.jpg)  | unet-WithoutReg | unet-WithoutReg |
| [检测+检测](rsp_json/det_det-rsp.json)             | [示例图片](./rsp_json/test_imgs/200dk-grey.jpg) | det_plus_det | det_plus_det |
| [检测+检测+分类](rsp_json/det_det_cls-rsp.json)    | [示例图片](./rsp_json/test_imgs/200dk-grey.jpg) | det_plus_det_cls | det_plus_det_cls |
| [检测+OCR](rsp_json/det_ocr-rsp.json)              | [示例图片](./rsp_json/test_imgs/200dk-grey.jpg) | det_plus_ocr | det_plus_ocr |
| [检测+分割](rsp_json/det_seg-rsp.json)             | [示例图片](./rsp_json/test_imgs/200dk-grey.jpg) | det_plus_seg | det_plus_seg |


# FAQ

## 启动服务时出现 cannot import name 'FileStorage'

原因: 这是因为 connexion 这个三方包依赖的 werkzeug 的api发生变化，connexion 这个三方包没有及时修改。

解决：flask文件中 from werkzeug import secure_filename,FileStorage 报错cannot import name 'FileStorage'

1. 如果改完出现cannot import name ‘FileStorage’ ,这是因为 FileStorage在werkzeug.datastructures 下面
    ```
    from werkzeug  import FileStorage
    ```
    改为
    ```
    from werkzeug.utils import secure_filename
    from werkzeug.datastructures import FileStorage
    ```
2. 如果出现cannot import secure_filename把from werkzeug 改为from werkzeug.utils即可
   

