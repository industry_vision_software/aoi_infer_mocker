#!/usr/bin/env python
# _*_coding:utf-8_*_

"""
opt customer dll package script

@Time : 2022/1/12 21:27
@Author: RunAtWorld
@File: package_plugin.py
"""
import os
import shutil


class PackagePlugins:
    """
    dll and language file package utils

    a. copy dll dll and language files from dev_dir to dir of sci/hw
    b. copy language files from dir of sci/hw to dir of sci/extension
    """

    def __init__(self, dev_build_path, dev_project_path, sci_customer_dll_path):
        self._dev_build_path = dev_build_path
        self._dev_project_path = dev_project_path
        self._sci_customer_dll_path = sci_customer_dll_path
        self._extensions_order_dic = {'10001': 'HW_AI_Login',
                                      '10002': 'HW_AI_PostImage',
                                      '10003': 'HW_AI_Segement',
                                      '10004': 'HW_AI_OCR',
                                      '10005': 'HW_AI_Det_Cls',
                                      '10006': 'HW_AI_ProductList',
                                      '10007': 'HW_AI_Det_Plus_Det',
                                      '10008': 'HW_AI_Det_Plus_Seg',
                                      '10009': 'HW_AI_Det_Plus_Ocr'
                                      }

    def copy_files(self, src_dir, target_dir, *files):
        if not os.path.exists(target_dir):
            os.makedirs(target_dir)
        for file in files:
            src_file = os.path.join(src_dir, file)
            target_file = os.path.join(target_dir, file)
            if os.path.exists(target_file):
                os.remove(target_file)
            print(src_file, "+==>", target_dir)
            shutil.copy(src_file, target_file)

    def copy_lang_qms(self, src_dir, target_dir):
        """
        copy language files
        """
        self.copy_files(src_dir, target_dir, "zh_CN.qm", "en.qm")

    def copy_dlls(self, src_dir, target_dir, dll_name):
        """
        copy dll files
        """
        self.copy_files(src_dir, target_dir, dll_name)

    def generate_packages(self):
        """
        generate dll packages in Sci Smart Root Dir
        """
        print("start to generate packages...")
        dll_files = [file for file in os.listdir(self._dev_build_path) if file.endswith(".dll")]
        for dll_file in dll_files:
            dll_name = dll_file[:dll_file.rindex(".dll")]
            target_dir = os.path.join(self._sci_customer_dll_path, dll_name)

            # copy dll file
            self.copy_dlls(self._dev_build_path, target_dir, dll_file)

            # copy language qm file
            src_qm_dir = os.path.join(self._dev_project_path, dll_name)
            self.copy_lang_qms(src_qm_dir, target_dir)
        print("finish to generate packages...")

    def copy_to_sci_extensions(self, extension_parent_dir):
        """
        copy dll and language qm to sci extensions dir
        """
        print("start to copy files to sci extensions...")
        for extern_num, dll_name in self._extensions_order_dic.items():
            target_extension_dir = os.path.join(extension_parent_dir, extern_num)
            src_customer_dll_dir = os.path.join(self._sci_customer_dll_path, dll_name)
            # copy language qm file
            self.copy_lang_qms(src_customer_dll_dir, target_extension_dir)
        print("finish to copy files to sci extensions...")


if __name__ == '__main__':
    dev_build_path = r'E:\atlas\opt_worksapce\src\ascend-industrial\opt_vision\x64\Release'
    project_path = r'E:\atlas\opt_worksapce\src\ascend-industrial\opt_vision'
    sci_customer_dll_path = r'D:\OPT\SciSmart3\HW'
    extension_dir = r'D:\OPT\SciSmart3\extension'
    pp = PackagePlugins(dev_build_path, project_path, sci_customer_dll_path)
    pp.generate_packages()
    pp.copy_to_sci_extensions(extension_dir)
